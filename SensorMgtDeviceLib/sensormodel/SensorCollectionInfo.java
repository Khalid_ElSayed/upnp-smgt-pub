package gem.sensormgt.sensormodel;

import java.util.HashMap;
import android.util.Log;

/**
 * Holds the data for a sensor collection 
 * Used to create SensorCollections dynamically and to read from/write to xml
 * 
 */

public class SensorCollectionInfo  {

	private static final String TAG = "SensorCollectionInfo";
	private static final boolean DEBUG = false;
	
//	private ArrayList<Sensor> mSensors = new ArrayList<Sensor>();
	private HashMap<String, String> fields = new HashMap<String, String>();

	private String mPath;
	
	private String mInstanceId;
	
	// member variables according to specs
	private String mSensorCollectionId="";
	private String mType="";
	private String mFriendlyName="";
	private String mInformation="";
	private String mUniqueId="";

	public interface SensorCollectionDataAvailableListener {
		public void onSensorCollectionDataAvailable(SensorCollectionInfo sensorCollection);
	}
	
	public interface SensorsChangedListener {
		public void onSensorsChanged(SensorCollectionInfo sensorCollection);
	}

	public SensorCollectionInfo(String path) {
		
		if (DEBUG) Log.d(TAG, "new SensorCollection "+path);
		
		this.mPath = path;
		this.mInstanceId = getInstanceIdFromPath(path);
	
	}
	
	/**
	 * Gets the instanceID, assumes path is correct and last segment is instanceId
	 */
	private String getInstanceIdFromPath(String path) {
		
		String[] segments = path.split("/");
		
		if (segments.length > 1)
			return segments[segments.length-1];
		else
			return "";
	}
	
	
	
//	public ArrayList<Sensor> getSensors() {
//		return mSensors;
//	}

	public String getPath() {
		return mPath;
	}
	
	public String getInstanceId() {
		return mInstanceId;
	}

	public String getSensorCollectionId() {
		return mSensorCollectionId;
	}

	public String getType() {
		return mType;
	}

	public String getFriendlyName() {
		return mFriendlyName;
	}

	public String getInformation() {
		return mInformation;
	}

	public String getUniqueId() {
		return mUniqueId;
	}
//
//	public int getSensorNumberOfEntries() {
//		return mSensorNumberOfEntries;
//	}
//

	
}
