
UPnP SensorManagement

This is an implementation of the UPnP SensorManagement and Datastore.
The specification is available at www.upnp.org

The SensorManagement Device and control point are implemented on Android based on the Cling UPnP Stack.
The Cling UPnP Stack is available at http://4thline.org/projects/cling/ under the LGPL license.

The archive consists of 5 eclipse projects for Android:

1) SensorMgtFridgeDevice project: 
The App implements the "Fridge" example as defined in the UPnP SensorManagement documentation. 
Additionally, it implements a Datastore. 
When installed on an Android device, it presents a simulated Fridge as a UPnP SensorManagement device on the network. 
The temperature and energy sensors produce random values. 
The device can be discovered and controlled using generic control points 
such as "UPnP Device Spy" and Cling Workbench and gupnp-universal-cp (linux gupnp-tools)

2) SensorMgtDeviceLib library project: 
This library implements the sensor management and datastore functions on top of the Cling UPnP stack. 
The SensorMgtFridgeDevice implements the UI and the simulated sensors, and links to this library for the UPnP functionality. 

3) SensorMgtFridgeControlPoint project: 
This App implements a user interface to access the sensors as exposed by the SensorMgtFridgeDevice. 
It discovers UPnP SensorManagement devices on the network, and displays detailed sensor info if 
the discovered device is a fridge. 
It subscribes to events to receive updates of the temperature sensors, and energy sensors. 
A long press on the temperature sensors shows a dialog to set the sensor update frequency on the device. 
The project implements the user interface, and uses the SensorMgtControlPointLib library for the UPnP functionality

4) SensorMgtControlPointLib library project: 
This library implements the sensormanagement and datastore control point functions on top of the Cling UPnP stack. 
It supports invoking the sensormanagement and datastore actions in an asynchronous and synchronous way. 
Additionally, it builds a local representation of the datamodel as exposed by a sensor management device. 

5) HomeAutomationControlPoint project: 
The app is an experimental user interface for home automation. 
It discovers UPnP SensorManagement Devices and combines sensor information from multiple devices
in one user interface. 
It uses the SensorMgtControlPointLib library to implement the UPnP Control point functions.


-----Building instructions-----
 
Requirements Android tools: Android ADT bundle/eclipse + jdk 

In eclipse select 
"File" -> "import...". 
In the import dialog select the "Git" tab, and select "Projects from Git" option
Select "URI" to import the Git archive from eclipse

In the "Source Git Repository" dialog enter
URI: https://bitbucket.org/gmekenkamp/upnp-smgt.git
select "Next>"  
select "master" branch, select "Next>"
select local destination (directory where sources will be installed)  

The 5 android projects will be cloned to the disk now.
After the files have been downloaded, the "import projects" wizard opens
Select "Next>"
Import projects opens with 5 projects selected. 
Keep all selected or select specific projects
(for example, just SensorMgtDeviceLib and SensorMgtFridgeDevice to build the device only)

Select "Finish"

Build and install on Android device.


