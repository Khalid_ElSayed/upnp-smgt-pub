/* Copyright (c) 2013, TP Vision Holding B.V. 
 * All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of TP Vision nor the  names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL TP VISION HOLDING B.V. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

package com.tpvision.uicomponent;


import com.tpvision.figaro.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;


//TODO: alignment gravity 

/**
 * Draws a circle as a view, allows setting its size and padding, and default color using attributes.  
 * Using the setColor function the circle is redrawn using the new color.
 * Allows for easy led like view for usage in a user interface. Can be inserted as a custom component in a layout.  
 *
 * @attr ref android.R.styleable#CircleView_circleColor
 * 
 */
public class CircleView extends View {
    
	//for debugging
	private static final String TAG = "CircleView"; 
	private static final boolean DEBUG = false;
	
	//default values
	private static final int DEFAULT_CIRCLE_SIZE = 30;
	private static final int DEFAULT_CIRCLE_COLOR = Color.GRAY;
	
	//computed values based on available space
	private int mComputedCircleSize;
	private int mExtraPaddingLeft;
	private int mExtraPaddingTop;
	
	private Paint mPaint; //to avoid re-creating each onDraw
	private int mCircleColor =  DEFAULT_CIRCLE_COLOR;
	
	/**
     * Construct object, initializing with any attributes we understand from a
     * layout file. 
     * 
     * @see android.view.View#View(android.content.Context, android.util.AttributeSet)
     */
	public CircleView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		//get color attribute
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleView);
		mCircleColor = a.getColor(R.styleable.CircleView_circleColor, DEFAULT_CIRCLE_COLOR);
		a.recycle();
		
		initCircleLed();
	}
	
	/**
     * Construct object, initializing with any attributes we understand from a
     * layout file. 
     * 
     * @see android.view.View#View(android.content.Context, android.util.AttributeSet)
     */
    public CircleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
    
    /**
     * Constructor for manual object creation
     * @param context
     */
    public CircleView(Context context) {
        this(context, null,0);
    }
    
    
    private final void initCircleLed() {
       	mPaint = new Paint();
    	mPaint.setAntiAlias(true);
    	mPaint.setStyle(Paint.Style.FILL);
    }

   
    /**
     * @see android.view.View#measure(int, int)
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    	
    	int width = measureWidth(widthMeasureSpec);
    	int height = measureHeight(heightMeasureSpec);
    	
    	if (DEBUG) Log.d(TAG,"onMeasure: "+ width +", "+ height);
        setMeasuredDimension(width, height);
    }
    
    /**
     * Determines the width of this view
     * @param measureSpec A measureSpec packed into an int
     * @return The width of the view, honoring constraints from measureSpec
     */
    private int measureWidth(int widthMeasureSpec) {
        int width = 0;
        int specMode = MeasureSpec.getMode(widthMeasureSpec);
        int specwidth = MeasureSpec.getSize(widthMeasureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            // We were told how big to be
            width = specwidth;    
        } else {
        	int ledsize = DEFAULT_CIRCLE_SIZE;
            width = (int) getPaddingLeft() + getPaddingRight() + ledsize; 
            
            if (specMode == MeasureSpec.AT_MOST) {
                // Respect AT_MOST value if that was what is called for by measureSpec
                width = Math.min(width, specwidth);      
            }
        }
        return width;
    }

    /**
     * Determines the height of this view
     * @param measureSpec A measureSpec packed into an int
     * @return The height of the view, honoring constraints from measureSpec
     */
    private int measureHeight(int heightMeasureSpec) {
        int height = 0;
        int specMode = MeasureSpec.getMode(heightMeasureSpec);
        int specheight = MeasureSpec.getSize(heightMeasureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            // We were told how big to be
            height = specheight;     
        } else {
        	int ledsize = DEFAULT_CIRCLE_SIZE;
            height = (int) getPaddingTop() + getPaddingBottom() + ledsize; 
            
            if (specMode == MeasureSpec.AT_MOST) {
                // Respect AT_MOST value if that was what is called for by measureSpec
                height = Math.min(height, specheight);      
            }
        }
        return height;
    }
    
    /**
     * @see android.view.View#onSizeChanged(int, int)
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    	 if (DEBUG) Log.d(TAG,"onSizeChanged: "+w+", "+h);
    	 
    	 computeCircleDimensions(w,h);
    }
    
    /**
     * Computes the maximum size of the circle, and calculates extra padding according to alignment of the circle 
     * @param w width
     * @param h height
     */
    protected void computeCircleDimensions(int w, int h) {
    
	    int width = w - getPaddingLeft() - getPaddingRight();
	   	int height = h - getPaddingTop() - getPaddingBottom();	
	    
	   	//take minimum space for circle diameter
	   	mComputedCircleSize = Math.min(width, height); 
	    
	   	
	   	//Gravity Center
	   	mExtraPaddingLeft = (width - mComputedCircleSize)/2; 		
	    mExtraPaddingTop = (height - mComputedCircleSize)/2;
	    
	   	Log.v(TAG,"Led Size: "+mComputedCircleSize);
    }
    
    
    public int getCircleSize() {
    	return mComputedCircleSize;
    }
    
    
    /**
     * Sets the color of the circle
     * @param color 
     */
    public void setColor(int color) {
    	if (DEBUG) Log.d(TAG,"setColor: "+color);
    	
    	mCircleColor = color;
    	invalidate();
    }
    
    
    /**
     * Draw the circle
     * 
     * @see android.view.View#onDraw(android.graphics.Canvas)
     */
    @Override
    protected void onDraw(Canvas canvas) {
    	super.onDraw(canvas);

    	float radius =  mComputedCircleSize/2;

    	float cx = getPaddingLeft() + mExtraPaddingLeft + radius;
    	float cy = getPaddingTop() + mExtraPaddingTop+ radius;

    	mPaint.setColor(mCircleColor);
     	   
    	if (mComputedCircleSize > 0)
    		canvas.drawCircle(cx, cy, radius, mPaint); 
    }
    
    
}
