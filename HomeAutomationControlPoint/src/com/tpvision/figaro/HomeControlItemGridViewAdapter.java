/* Copyright (c) 2013, TP Vision Holding B.V. 
 * All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of TP Vision nor the  names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL TP VISION HOLDING B.V. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

package com.tpvision.figaro;


import com.tpvision.figaro.HomeControlItem.DataChangeListener;
import com.tpvision.figaro.HomeControlItem.DeviceTypeEnum;
import com.tpvision.sensormgt.upnpcontrolpoint.model.DataItem;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;

public class HomeControlItemGridViewAdapter extends BaseAdapter implements DataChangeListener 
{        
    private Context context;  
    public HomeControlItems items;
    public Boolean isBindModeEnabled = false;
    public HomeControlItem itemToBind=null;
    
	public HomeControlItemGridViewAdapter(final Context context, HomeControlItems items) 
    {
    	super();
        this.context = context;  
        this.items = items;
        items.setOnDataChangedListener(this);
    }         
    
	@Override
	public final synchronized long getItemId(final int position) 
    {
        return position;
    }
	
	@Override
	public int getCount() 
    {
        return items.items.size();
    }

	@Override
    public Object getItem(int position) 
    {
        return items.items.get(position);
    }    
    
    @Override
    public int getViewTypeCount() {
        return 3;
    }
    
    @Override
    public int getItemViewType(int position) {
    	
    	HomeControlItem item = items.items.get(position);
    	if (item.getDeviceType()==DeviceTypeEnum.FRIDGE) {
    		return 2;
    	}
    	if (item.getDeviceType()==DeviceTypeEnum.ENERGY) {
    		return 1;
    	}
    		
    	return 0;
    }
    
    @Override
    public synchronized View getView(final int position, View convertView, final ViewGroup parent) 
    {
    	HomeControlItem item = items.items.get(position);
//    	if (this.isBindModeEnabled)
//    	{
//    		return item.getView(context, convertView, null, null, this.itemToBind);
//    	}
    	View view;
    	switch (item.getDeviceType()) {
    	case FRIDGE:
			view = ((HomeControlItemFridge) item).getView(context, convertView, null, null, null);
			break;
    		case ENERGY:
    			view = ((HomeControlItemEnergy) item).getView(context, convertView, null, null, null);
    			break;
    		default:
    			view = item.getView(context, convertView, null, null, null);
    			break;
    	}
    	
    	return view;
    }

    @Override
    public void onDataChanged(DataItem dataItem) {


    	String descr = dataItem.getDescription();
    	if (descr!=null) {
    		if (descr.contains("itemname=\"DoorOpenAlarm\"")) {
    			if (dataItem.getValue().contentEquals("1")) {
    				
    				((Activity)context).runOnUiThread(new Runnable() {
    	    			public void run() { 
    	    				Toast toast = Toast.makeText(context, "Don't leave the virtual door open\nYou'll get a huge virtual bill.", Toast.LENGTH_SHORT);
    	    				toast.show();
    	    			}
    	    			
    	    		});
    				
    			}
    		} else {

        		((Activity)context).runOnUiThread(new Runnable() {
        			public void run() { 
        				notifyDataSetChanged(); 
        			}
        		});
        	}
    	} 
    }
	
	
}



