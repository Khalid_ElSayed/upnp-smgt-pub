/* Copyright (c) 2013, TP Vision Holding B.V. 
 * All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of TP Vision nor the  names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL TP VISION HOLDING B.V. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

package com.tpvision.figaro;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;

public class BindingDialog extends Dialog
{
    public HomeControlItem homeControlItem;
    private HomeControlItemGridViewAdapter homeControlItemGridViewAdapter;	
	private GridView bindingsGridView;
	private Context context;
	
	public BindingDialog(Context context, HomeControlItem homeControlItem, HomeControlItems items) 
	{
		super(context);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.setContentView(R.layout.item_bindings_dialog);
		this.setCanceledOnTouchOutside(true);		
		
		this.context = context;
		this.homeControlItem = homeControlItem;
		items.items.remove(homeControlItem);
		this.homeControlItemGridViewAdapter = new HomeControlItemGridViewAdapter(this.context, items);
		this.homeControlItemGridViewAdapter.isBindModeEnabled = true;
		this.homeControlItemGridViewAdapter.itemToBind = homeControlItem;		
		
		this.bindingsGridView = (GridView)this.findViewById(R.id.bindingsGridView);
		this.bindingsGridView.setOnItemClickListener(gridViewGalleryContentItemClick);
		this.bindingsGridView.setAdapter(this.homeControlItemGridViewAdapter);		
		
		View itemToBind = null;
		itemToBind = homeControlItem.getView(context, itemToBind, null, null, null);
		View item = ((View)findViewById(R.id.homeControlItem));
		((ImageView)item.findViewById(R.id.itemImageView)).setImageDrawable(((ImageView)itemToBind.findViewById(R.id.itemImageView)).getDrawable());		
		((TextView)item.findViewById(R.id.friendlyNameTextView)).setText(((TextView)itemToBind.findViewById(R.id.friendlyNameTextView)).getText());
		
	}
	
	 private OnItemClickListener gridViewGalleryContentItemClick = new OnItemClickListener() 
	 {		
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
		{		
			HomeControlItem item = (HomeControlItem)homeControlItemGridViewAdapter.getItem(position);
			if (homeControlItem.isBindedToItem(item))
			{
				if (homeControlItem.unbindFromItem(item))
				{
					homeControlItemGridViewAdapter.notifyDataSetChanged();
				}
			}
			else
			{
				if (homeControlItem.canBindToItem(item))
				{
					if (homeControlItem.bindToItem(item))
					{
						homeControlItemGridViewAdapter.notifyDataSetChanged();
					}
				}
			}
		}
	};
}
