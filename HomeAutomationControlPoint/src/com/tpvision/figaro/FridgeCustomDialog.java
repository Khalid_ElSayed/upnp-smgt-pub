/* Copyright (c) 2013, TP Vision Holding B.V. 
 * All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of TP Vision nor the  names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL TP VISION HOLDING B.V. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

package com.tpvision.figaro;

import com.tpvision.figaro.HomeControlItem.DeviceTypeEnum;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;


public class FridgeCustomDialog extends Dialog
{
	private EditText friendlyNameEditText;		
    public HomeControlItemFridge homeControlItem;	
	public rgbControls rgbControl;
	private Context context;
	private RelativeLayout dialogRelativeLayout;
	private Dialog changeFriendlyNameDialog;
	private View itemView = null;
	
	public FridgeCustomDialog(Context context, HomeControlItemFridge homeControlItem) 
	{
		super(context);
		this.context = context;
		this.homeControlItem = homeControlItem;
		this.setCanceledOnTouchOutside(true);		
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.action_dialog);		
		this.dialogRelativeLayout = (RelativeLayout)findViewById(R.id.dialogRelativeLayout);		
		this.createHomeControlItemUserInterface(homeControlItem);		
	}
	
	private void createHomeControlItemUserInterface(HomeControlItem homeControlItem)
	{		
		itemView = homeControlItem.getView(context, itemView, friendlyNameTextViewOnLongClick, itemOnClickListener, null);
		                
        LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);        
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        this.dialogRelativeLayout.addView(itemView, layoutParams);
        
       addTemperatureSensors();
	}
	
	private View.OnClickListener itemOnClickListener = new View.OnClickListener() {
		
		
		public void onClick(View v) 
		{						
			homeControlItem.activate();
			/*itemView = homeControlItem.getView(context, itemView, friendlyNameTextViewOnLongClick,itemOnClickListener, null);
			itemView.invalidate();*/
		}
	}; 
	
	private OnLongClickListener friendlyNameTextViewOnLongClick = new OnLongClickListener() 
	{
		
		public boolean onLongClick(View v) 
		{
			changeFriendlyNameDialog = new Dialog(context);
			changeFriendlyNameDialog.setContentView(R.layout.change_name_dialog);			
			
			friendlyNameEditText = (EditText)changeFriendlyNameDialog.findViewById(R.id.friendlyNameEditText);
			friendlyNameEditText.setText(homeControlItem.friendlyname);
			
			((Button)changeFriendlyNameDialog.findViewById(R.id.okButton)).setOnClickListener(okButtonClicked);
			((Button)changeFriendlyNameDialog.findViewById(R.id.cancelButton)).setOnClickListener(cancelButtonClicked);
			changeFriendlyNameDialog.setCanceledOnTouchOutside(true);
			changeFriendlyNameDialog.show();
			return true;
		}
	};
	private TextView mTemperatureText;	
	
	private void addTemperatureSensors() {
		mTemperatureText = new TextView(this.context);
		mTemperatureText.setTextSize(24);
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);        		                
        layoutParams.addRule(RelativeLayout.ALIGN_TOP, R.id.home_control_item_layout);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        layoutParams.addRule(RelativeLayout.RIGHT_OF, R.id.home_control_item_layout);
        layoutParams.leftMargin = 20;
        this.dialogRelativeLayout.addView(this.mTemperatureText, layoutParams);
        mTemperatureText.setText(homeControlItem.getTempSensors());
	}

	private void addRGBcontrol(HomeControlItem homeControlItem)
	{
		this.rgbControl = new rgbControls(this.context, homeControlItem);
		
		LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);        		                
        layoutParams.addRule(RelativeLayout.ALIGN_TOP, R.id.home_control_item_layout);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        layoutParams.addRule(RelativeLayout.RIGHT_OF, R.id.home_control_item_layout);
        layoutParams.leftMargin = 20;
        this.dialogRelativeLayout.addView(this.rgbControl, layoutParams);
        if (homeControlItem.mDeviceType == DeviceTypeEnum.LIGHT_LIVING_COLORS)
        {
        	this.rgbControl.setOnRgbValueChangedListener(onRgbValueChangedListener);
        }
	}
	
	private OnRgbValueChangedListener onRgbValueChangedListener = new OnRgbValueChangedListener() 
	{
		
		public void OnRgbValueChanged(ColorRGB newValue) 
		{		
			itemView = homeControlItem.getView(context, itemView, friendlyNameTextViewOnLongClick, itemOnClickListener, null);
			itemView.invalidate();
			homeControlItem.switchOn();
		}
	}; 
	
	private void addOnOfSwitch(HomeControlItem homeControlItem)
	{
		//can also have RGB payload
	}
	
	private void addToggleSwitch(HomeControlItem homeControlItem)
	{
		
	}
	
	public View.OnClickListener okButtonClicked = new View.OnClickListener() 
	{		
		public void onClick(View v) 
		{
			String newName = friendlyNameEditText.getText().toString(); 
			homeControlItem.changeFriendlyName(newName);
			changeFriendlyNameDialog.dismiss();			
		}
	}; 
	
	public View.OnClickListener cancelButtonClicked = new View.OnClickListener() 
	{
		
		public void onClick(View v) 
		{
			changeFriendlyNameDialog.dismiss();
		}
	};
	
	public void updateUI()
	{
		this.itemView = this.homeControlItem.getView(context, itemView, friendlyNameTextViewOnLongClick, itemOnClickListener, null);
		this.itemView.invalidate();
		if (homeControlItem.supportsRGB())
		{
			this.rgbControl.updateUI();
		}
	}
}
