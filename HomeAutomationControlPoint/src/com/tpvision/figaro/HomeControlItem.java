/* Copyright (c) 2013, TP Vision Holding B.V. 
 * All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of TP Vision nor the  names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL TP VISION HOLDING B.V. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

package com.tpvision.figaro;

import static java.util.Arrays.asList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.tpvision.figaro.ActionParameter.ParameterIdEnum;
import com.tpvision.figaro.SupportedAction.HA_ActionTypeEnum;
import com.tpvision.sensormgt.upnpcontrolpoint.model.DataItem;
import com.tpvision.sensormgt.upnpcontrolpoint.model.DataItem.SensorDataItemReadListener;
import com.tpvision.sensormgt.upnpcontrolpoint.model.Sensor;
import com.tpvision.sensormgt.upnpcontrolpoint.model.SensorCollection;
import com.tpvision.sensormgt.upnpcontrolpoint.model.SensorURN;


import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

//@Element(name="Node", required=true)
@Root(name="Node")
public class HomeControlItem implements SensorDataItemReadListener {
	
	public interface DataChangeListener {
		public void onDataChanged(DataItem dataItem);
	}
	
	public enum DeviceTypeEnum 
	{	
		LIGHT,
		LIGHT_LIVING_COLORS,
		SWITCH,
		DIMMER,
		PUSH_SWITCH,
		ENERGY,
		FRIDGE, 
		OTHER
	};
	
	public enum DeviceBindingTypeEnum 
	{	
		SOURCE,
		SINK,
		DUAL
	};
		
	public enum CN_Networks_Enum
	{
		RF4CENetwork,
		ZigBee
	}

	private static final String TAG = null;

	private static final String BASICONOFF = "Switch";
	
	public static int[] DeviceTypeResourcesOff = new int[] {	
														R.drawable.item_lamp_off,
														R.drawable.item_living_colors_off,
														R.drawable.item_switch_off,
														R.drawable.item_dimmer_off,
														R.drawable.item_push_switch_off,
														R.drawable.item_other_off
													};
	
	public static int[] DeviceTypeResourcesOn = new int[] {	
														R.drawable.item_lamp_on,
														R.drawable.item_living_colors_on,
														R.drawable.item_switch_on,
														R.drawable.item_dimmer_on,
														R.drawable.item_push_switch_off,
														R.drawable.item_other_on
													};
	
	public static int[] LivingColorsResourcesOn = new int[] {
														R.drawable.living_colors_0,
														R.drawable.living_colors_1,
														R.drawable.living_colors_2,
														R.drawable.living_colors_3,
														R.drawable.living_colors_4,
														R.drawable.living_colors_5,
														R.drawable.living_colors_6,
														R.drawable.living_colors_7,
														R.drawable.living_colors_8,
														R.drawable.living_colors_9,
														R.drawable.living_colors_10
													};
	
	public static ColorRGB[] LivingColorsResourcesOnRGB = new ColorRGB[] { 
														new ColorRGB(251,232,67),
														new ColorRGB(252,195,0),
														new ColorRGB(243,218,234),
														new ColorRGB(233,175,207),														
														new ColorRGB(240,148,146),														
														new ColorRGB(179,223,234),														
														new ColorRGB(142,208,242),														
														new ColorRGB(221,226,141),														
														new ColorRGB(194,210,76),														
														new ColorRGB(165,204,123),
														new ColorRGB(250,250,250)
													};
	
	@Attribute(name="id")
	public String nodeId;	
	
	@Attribute(name="devicetype")
	public DeviceTypeEnum mDeviceType=DeviceTypeEnum.OTHER;
	
	@Attribute(name="friendlyname")
	public String friendlyname;
	
	@Attribute(name="bindingtype", required=false)
	public DeviceBindingTypeEnum deviceBindingType;	
	
	@ElementList(name="Commands", required=false)
	public List<SupportedAction> supportedActions;			//actions supported by the device
	
	@ElementList(name="Bindings", entry="NodeId", required=false)
	public List<String> bindedNodesIds;
	
	private boolean isOn = false;
	private Boolean supportsTogle = false;
	private Boolean supportsOnOff = false;
	private Boolean supportsRGB = false;
	private Boolean actionsProcessed = false;

	private Serializer serializer = null;

	private SensorCollection mSensorCollection;

	private String mSensorID;
	private String mSensorURN;

	private DataItem mOnOffDataItem;

	private HomeControlItems mParent;

	private DataChangeListener mListener;
	
	public HomeControlItem(String nodeId, DeviceTypeEnum devicetype, DeviceBindingTypeEnum deviceBindingType, String friendlyname, List<SupportedAction> supportedActions, List<String> bindedNodesIds) 
	{
		super();
		this.init(nodeId, devicetype, deviceBindingType, friendlyname);
		this.supportedActions = supportedActions;
		this.bindedNodesIds = bindedNodesIds;
		
		//FIXME: HACK
		actionsProcessed = false;
		supportsOnOff = true;
		
	}
	
	public HomeControlItem(HomeControlItems parent, SensorCollection sc, String nodeId, DeviceTypeEnum devicetype, DeviceBindingTypeEnum deviceBindingType, String friendlyname) {
		this(nodeId, devicetype,deviceBindingType, friendlyname);
		mSensorCollection = sc;	
		mParent = parent;
		if (devicetype == DeviceTypeEnum. LIGHT)
		{
			mOnOffDataItem = findBasicOnOffSensor();
			if (mOnOffDataItem!=null) {
				mOnOffDataItem.addSensorDataItemReadListener(this);
				mOnOffDataItem.readSensorData();
			}
		}
		
	}

	public HomeControlItem(String nodeId, DeviceTypeEnum devicetype, DeviceBindingTypeEnum deviceBindingType, String friendlyname)
	{
		super();		
		this.init(nodeId, devicetype, deviceBindingType, friendlyname);		
	}
	
	public HomeControlItem()
	{
		super();
		this.init("", DeviceTypeEnum.OTHER, DeviceBindingTypeEnum.DUAL, "");
	}
	
	private void init(String nodeId, DeviceTypeEnum devicetype, DeviceBindingTypeEnum deviceBindingType, String friendlyname)
	{
		this.serializer = new Persister();
		this.nodeId = nodeId;
		this.mDeviceType = devicetype;
		this.friendlyname = friendlyname;
		this.supportedActions = new ArrayList<SupportedAction>();
		this.bindedNodesIds = new ArrayList<String>();
		this.deviceBindingType = deviceBindingType;
		
		
	}
	
	public DeviceTypeEnum getDeviceType() {
		return mDeviceType;
	}
	
	public SensorCollection getSensorCollection() {
		return mSensorCollection;
	}
	
	public DataItem findDataItem(String itemName) {
		
		ArrayList<Sensor> sensors = mSensorCollection.getSensors();
		Iterator<Sensor> si = sensors.iterator();
		boolean found = false;
		DataItem dataItem=null;
		while (si.hasNext()&&!found) {
			Sensor sensor = si.next();
			ArrayList<SensorURN> sensorURNs = sensor.getSensorURNs();
			Iterator<SensorURN> su = sensorURNs.iterator();
			while (su.hasNext()&&!found) {
				SensorURN sensorURN = su.next();
				ArrayList<DataItem> dataItems = sensorURN.getDataItems();
				Iterator<DataItem> dii = dataItems.iterator();
				while (dii.hasNext()&&!found) {
					dataItem = dii.next();
					found = dataItem.getName().contentEquals(itemName);
				}
			}
		}
		
		return dataItem;
	}
	
	public DataItem findBasicOnOffSensor() {
		
		//in this sensor collection, locate the DataItem that implements a basic on/off 
		return findDataItem(BASICONOFF);
		
	}
	
	
//	public void initUPnPActions(UPnPControlPoint controlpoint, String currentUPnPDeviceUdn)
//	{
//		this.controlpoint = controlpoint;
//		this.currentUPnPDeviceUdn = currentUPnPDeviceUdn;
//		this.subscribeForUPnPEvents();
//	}
	
	public boolean updateNodeInfo()
	{
//		String nodeInfo = controlpoint.getNodeInfo(currentUPnPDeviceUdn, CN_Networks_Enum.RF4CENetwork.name(), this.nodeId);
//		try {
//			HomeControlItem homeControlItem = serializer.read(HomeControlItem.class, nodeInfo);
//			this.bindedNodesIds = homeControlItem.bindedNodesIds;
//			this.friendlyname = homeControlItem.friendlyname;
//			this.isOn = homeControlItem.isOn;
//			this.supportedActions = homeControlItem.supportedActions;
//			return true;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		return false;
	}
	
	public boolean canBindToItem(HomeControlItem homeControlItem)
	{
		if (this.deviceBindingType == homeControlItem.deviceBindingType) //items of the same type can't be binded
		{
			return false;
		}
		return true;
	}
	
	public boolean isBindedToItem(HomeControlItem homeControlItem)
	{	
		if (this.canBindToItem(homeControlItem))
		{					
			for (int i=0; i<bindedNodesIds.size();i++)
			{
				if (bindedNodesIds.get(i).equalsIgnoreCase(homeControlItem.nodeId))
				{
					return true;
				}
			}
		
			for (int i=0; i<homeControlItem.bindedNodesIds.size();i++)
			{
				if (homeControlItem.bindedNodesIds.get(i).equalsIgnoreCase(this.nodeId))
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean bindToItem(HomeControlItem homeControlItem)
	{	
//		if (this.canBindToItem(homeControlItem))
//		{
//			if (!isBindedToItem(homeControlItem))
//			{	
//				//TODO: Add upnp call to bind devices
//				if (homeControlItem.deviceBindingType == DeviceBindingTypeEnum.SOURCE | homeControlItem.deviceBindingType == DeviceBindingTypeEnum.DUAL)
//				{
//					if (this.controlpoint.sendMessage(this.currentUPnPDeviceUdn, CN_Networks_Enum.RF4CENetwork.name(), 
//							homeControlItem.nodeId, HA_ActionTypeEnum.BIND.name(), this.nodeId))
//					{
//						return homeControlItem.bindedNodesIds.add(this.nodeId);
//					}
//				}else
//				{
//					if (this.controlpoint.sendMessage(this.currentUPnPDeviceUdn, CN_Networks_Enum.RF4CENetwork.name(), 
//							this.nodeId, HA_ActionTypeEnum.BIND.name(), homeControlItem.nodeId))
//					{
//						return bindedNodesIds.add(homeControlItem.nodeId);
//					}
//				}				
//			}
//		}
		return false;
	}
	
	public boolean unbindFromItem(HomeControlItem homeControlItem)
	{
//		if (isBindedToItem(homeControlItem))
//		{
//			//TODO: Add upnp call to unbind devices
//			if (homeControlItem.deviceBindingType == DeviceBindingTypeEnum.SOURCE | homeControlItem.deviceBindingType == DeviceBindingTypeEnum.DUAL)
//			{
//				if (this.controlpoint.sendMessage(this.currentUPnPDeviceUdn, CN_Networks_Enum.RF4CENetwork.name(), 
//						homeControlItem.nodeId, HA_ActionTypeEnum.UNBIND.name(), this.nodeId))
//				{
//					return homeControlItem.bindedNodesIds.remove(this.nodeId);
//				}
//			}else
//			{
//				if (this.controlpoint.sendMessage(this.currentUPnPDeviceUdn, CN_Networks_Enum.RF4CENetwork.name(), 
//						this.nodeId, HA_ActionTypeEnum.UNBIND.name(), homeControlItem.nodeId))
//				{
//					return bindedNodesIds.remove(homeControlItem.nodeId);
//				}
//			}
//		}
		return false;
	}
	
	public void processActions()
	{
		Boolean supportsOn = false;
		Boolean supportsOff = false;
		for (int i=0;i<supportedActions.size(); i++)
		{
			if (supportedActions.get(i).payloadFormat.toUpperCase().contains("R,G,B"))
			{
				this.supportsRGB = true; 				
			}			
			
			if (supportedActions.get(i).isPayloadOptional)
			{
				if (supportedActions.get(i).getActionType() == HA_ActionTypeEnum.ON)
				{
					supportsOn = true;
				}else if (supportedActions.get(i).getActionType() == HA_ActionTypeEnum.OFF)
				{
					supportsOff = true;
				}
				if (supportedActions.get(i).getActionType() == HA_ActionTypeEnum.TOGGLE)
				{
					this.supportsTogle = true;
				}
			}
			if ((supportedActions.get(i).getCurrentValue(ParameterIdEnum.R) > 0) |
				(supportedActions.get(i).getCurrentValue(ParameterIdEnum.G) > 0) |
				(supportedActions.get(i).getCurrentValue(ParameterIdEnum.B) > 0))
			{
				this.isOn = true;
			}
		}
		this.supportsOnOff = supportsOn & supportsOff;
		this.actionsProcessed = true;
	}
	
	public Boolean changeFriendlyName(String newName)
	{		
//		if (this.controlpoint!=null)
//		{			
//			if (this.controlpoint.sendMessage(this.currentUPnPDeviceUdn, CN_Networks_Enum.RF4CENetwork.name(), 
//					this.nodeId, HA_ActionTypeEnum.CHANGE_NAME.name(), newName))
//			{
//				this.friendlyname = newName;
//				return true;
//			}
//		}
		return false;
	}
	
	public Boolean subscribeForUPnPEvents() {		
//		if (this.controlpoint!=null)
//		{			
//			return this.controlpoint.sendMessage(this.currentUPnPDeviceUdn, CN_Networks_Enum.RF4CENetwork.name(), 
//					this.nodeId, HA_ActionTypeEnum.SUBSCRIBE_EVENTS.name(), ".");			
//		}
		return false;
	}
	
	public Boolean supportsTogle()
	{	
		if (!actionsProcessed)
		{
			this.processActions();
		}
		return this.supportsTogle;
	}
	
	public Boolean supportsOnOff()
	{	
		if (!actionsProcessed)
		{
			this.processActions();
		}
		return this.supportsOnOff;
	}
	
	public Boolean supportsRGB()
	{
		if (!actionsProcessed)
		{
			this.processActions();
		}
		return this.supportsRGB;
	}
	
	public boolean activate() {
		return toggle();
	}
	

	public Boolean toggle()
	{
		if (this.supportsTogle())
		{
			if (this.sendUPnPAction(HA_ActionTypeEnum.TOGGLE))
			{			
				//this.isOn = !this.isOn;
				return true;
			}
		}
		else if (this.isOn)
		{
			return switchOff();
		}
		else if (!this.isOn)
		{
			return switchOn();
		}
		return false;
	}
	
	public boolean switchOn() {
		
		if (mOnOffDataItem!=null) {
		//	mOnOffDataItem.setValue("1");
			mOnOffDataItem.writeSensorData("1");
			mOnOffDataItem.readSensorData(); //triggers reading the value back, which should cause an asynchronous update response
			isOn = true;
		}
		return true;
	}
	
	public boolean switchOff() {
		if (mOnOffDataItem!=null) {
			//mOnOffDataItem.setValue("0");
			mOnOffDataItem.writeSensorData("0");
			mOnOffDataItem.readSensorData(); //triggers reading the value back, which should cause an asynchronous update response
			isOn = false;
		}
		return true;
	}
	
	
	
	
	private boolean sendUPnPAction(HA_ActionTypeEnum actionType)
	{
//		if (this.controlpoint!=null)
//		{			
//			if (this.controlpoint.sendMessage(this.currentUPnPDeviceUdn, CN_Networks_Enum.RF4CENetwork.name(), 
//					this.nodeId,actionType.name(), SupportedAction.getFormatedPayload(this.supportedActions, actionType)))
//			{
//				return true;
//			}
//		}
		return false;
	}
	
	public double getMaxValue(ParameterIdEnum parameter)
	{
		try
		{
			for (int i=0;i<supportedActions.size(); i++)
			{
				List <ActionParameter> parameters = supportedActions.get(i).parameters;
				for (int j=0; j<parameters.size(); j++)
				{
					if (parameters.get(i).id == parameter)
					{
						return parameters.get(i).maxValue;
					}
				}
			}
		}catch(Exception e)
		{}
		return -1;
	}
	
	public double getCurrentValue(ParameterIdEnum parameter)
	{
		try
		{
			for (int i=0;i<supportedActions.size(); i++)
			{
				double currentValue = supportedActions.get(i).getCurrentValue(parameter);
				if (currentValue>=0)
				{
					return currentValue;
				}								
			}
		}catch(Exception e)
		{}
		return -1;
	}
	
	public void setCurrentValue(ParameterIdEnum parameter, double value)
	{
		try
		{
			for (int i=0;i<supportedActions.size(); i++)
			{
				supportedActions.get(i).setCurrentValue(parameter, value);
			}
		}catch(Exception e)
		{}
	}

	public static int getItemPosByNodeId(List<HomeControlItem> homeControlItems, String nodeId)
	{	
		try
		{
			for (int i=0;i<homeControlItems.size();i++)
			{
				if (homeControlItems.get(i).nodeId.equalsIgnoreCase(nodeId))
				{
					return i;
				}
			}
		}catch(Exception e)
		{}
		return -1;
	}
		
	public View getView(Context context, View convertView, OnLongClickListener friendlyNameTextViewOnLongClick, OnClickListener itemOnClickListener, HomeControlItem itemToBind) 
    {    
		try
		{
	        ImageView itemImageView;
	        TextView friendlyNameTextView;
	        
	        if (convertView == null) 
	        {
	        	LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);        	
	        	convertView = inflater.inflate(R.layout.home_control_item, null);
	        }
	                
	        itemImageView = (ImageView)convertView.findViewById(R.id.itemImageView);
	        
	        
	        friendlyNameTextView = (TextView)convertView.findViewById(R.id.friendlyNameTextView);        
	        friendlyNameTextView.setText(this.friendlyname);
	        if (friendlyNameTextViewOnLongClick!=null)
	        {
	        	friendlyNameTextView.setOnLongClickListener(friendlyNameTextViewOnLongClick);
	        }
	        if(itemOnClickListener!=null)
	        {
	        	itemImageView.setOnClickListener(itemOnClickListener);
	        }
	        if(itemToBind!=null)
	        {
	        	ImageView itemOverlayNotBindedImageView = (ImageView)convertView.findViewById(R.id.itemOverlayNotBindedImageView);
	        	ImageView itemOverlayNoBindingImageView = (ImageView)convertView.findViewById(R.id.itemOverlayNoBindingImageView);
	        	itemOverlayNoBindingImageView.setVisibility(View.INVISIBLE);
	        	itemOverlayNotBindedImageView.setVisibility(View.INVISIBLE);
	        	if (this.canBindToItem(itemToBind))
	        	{        		
	        		if (!this.isBindedToItem(itemToBind))
	        		{
	        			itemOverlayNotBindedImageView.setVisibility(View.VISIBLE);
	        		}
	        	}else
	        	{
	        		itemOverlayNoBindingImageView.setVisibility(View.VISIBLE);
	        	}        	
	        }
	        
	        //access dataItem to update the isOn value
	        this.isOn= (mOnOffDataItem!=null) && (mOnOffDataItem.getValue()!=null) && (mOnOffDataItem.getValue().contentEquals("1"));
	        
	        if (this.isOn)
	        {
	        	if (this.mDeviceType == DeviceTypeEnum.LIGHT_LIVING_COLORS)
	        	{
	        		int R = (int)this.getCurrentValue(ParameterIdEnum.R);
	        		int G = (int)this.getCurrentValue(ParameterIdEnum.G);
	        		int B = (int)this.getCurrentValue(ParameterIdEnum.B);
	        	
	        		int max = Math.max(Math.max(R, G), B);		
	        		if (max > rgbControls.MAX_COLOR_COMPONENT)
	        		{
	        			double brightness = 0;        			
	        			if (R==max)
	        			{
	        				brightness = R/rgbControls.MAX_COLOR_COMPONENT;
	        				R = rgbControls.MAX_COLOR_COMPONENT;				
	        				G = (int)(G/brightness);
	        				B = (int) (B/brightness);
	        			}else if (G==max)
	        			{
	        				brightness = G/rgbControls.MAX_COLOR_COMPONENT;
	        				R = (int) (R/brightness);				
	        				G = rgbControls.MAX_COLOR_COMPONENT;
	        				B = (int) (B/brightness);
	        			}else if (B==max)
	        			{
	        				brightness = B/rgbControls.MAX_COLOR_COMPONENT;
	        				R = (int) (R/brightness);				
	        				G = (int) (G/brightness);
	        				B = rgbControls.MAX_COLOR_COMPONENT;
	        			}        			
	        		}
	        		
	        		ColorRGB color = new ColorRGB(R, G, B);
	        		int pos=0;
	        		double minDist =100000;
	        		for (int i=0; i < LivingColorsResourcesOnRGB.length; i++)
	        		{
	        			double dist = color.DistanceToColor(LivingColorsResourcesOnRGB[i]);
	        			if (dist<minDist)
	        			{
	        				minDist = dist;
	        				pos = i;
	        			}
	        		}
	        		itemImageView.setImageResource(HomeControlItem.LivingColorsResourcesOn[pos]);

	        		
	        		/*itemImageView.setImageResource(HomeControlItem.DeviceTypeResourcesOff[this.devicetype.ordinal()]);
	        		Drawable drawable = context.getResources().getDrawable(HomeControlItem.DeviceTypeResourcesOff[this.devicetype.ordinal()]);	        		
	        		Canvas canvas = new Canvas(((BitmapDrawable)drawable).getBitmap());
	        		Paint paint = new Paint();
	        		paint.setARGB(150, R, G, B);
	        		Matrix m = new Matrix();
	        		m.postRotate(45, canvas.getWidth()/2, canvas.getHeight()/2);
	        		m.postScale(1, 1);
	        		canvas.setMatrix(m);	        		
	        		canvas.drawOval(new RectF(0, 55, 100, 95), paint);	        		
	        		itemImageView.draw(canvas);*/	        		
	        		
	        	}else
	        	{
	        		itemImageView.setImageResource(HomeControlItem.DeviceTypeResourcesOn[this.mDeviceType.ordinal()]);
	        	}
	        }else
	        {
	        	itemImageView.setImageResource(HomeControlItem.DeviceTypeResourcesOff[this.mDeviceType.ordinal()]);
	        }        
	        
	        return convertView;
		}catch(Exception e)
		{}
		return null;
    }

	public boolean isOn() 
	{
		return this.isOn;
	}

	public void setOn(boolean isOn) 
	{
		this.isOn = isOn;
	}

	@Override
	public void onSensorDataItemRead(DataItem dataItem) {
		Log.w(TAG,"DataItem event recieved from "+dataItem.getName());
		isOn = dataItem.getValue().equals("1");
		mParent.onDataChanged(dataItem);
	}
	

	
}
