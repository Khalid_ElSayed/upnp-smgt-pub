/* Copyright (c) 2013, TP Vision Holding B.V. 
 * All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of TP Vision nor the  names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL TP VISION HOLDING B.V. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

package com.tpvision.figaro;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

public class DevicesDialog extends DialogFragment {

	// Debugging
	public final String TAG = this.getClass().getSimpleName(); //get the class name for debugging
	private static final boolean DEBUG = true;

	private List<UPnPDeviceInfo> mDevices;
	private ArrayList<UPnPDeviceInfo> mDevicesOriginal;
	private DevicesListAdapter mDeviceListAdapter;

	
	static DevicesDialog newInstance() {
		return new DevicesDialog();
    }

	public void setDiscoveredDevices(List<UPnPDeviceInfo> discoveredDevices) {
		mDevices = discoveredDevices;
		mDevicesOriginal = new ArrayList<UPnPDeviceInfo>(discoveredDevices);
		notifyDataSetChanged();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		//Get the layout inflater
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View layout = inflater.inflate(R.layout.dialog_devices_list, null);
		ListView listView = (ListView) layout.findViewById(R.id.listView1);
		mDeviceListAdapter = new DevicesListAdapter(getActivity(), mDevices);
		listView.setAdapter(mDeviceListAdapter);
		listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

		builder.setTitle(R.string.dialog_title_devices)
		.setView(layout);

		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

				//write the device preferences
				SharedPreferences sharedPreferences = getActivity().getPreferences(Activity.MODE_PRIVATE);
				SharedPreferences.Editor editor = sharedPreferences.edit();

				Set<String> preferredDevices = new HashSet<String>();
				for (UPnPDeviceInfo device: mDevices) {
					preferredDevices.add(device.getUDN()+"/"+device.getFriendlyName());
				}

				editor.putStringSet("UDN", preferredDevices);
				editor.commit();
				
				//notify the host application of this dialog of the change
				mListener.onSelectionUpdated(DevicesDialog.this);
			}
		});

		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// User cancelled the dialog, restore the original settings
				mDevices = mDevicesOriginal;
				mDevicesOriginal = null;
				
				notifyDataSetChanged();
				
				//notify the host application of this dialog of the change
				mListener.onSelectionUpdated(DevicesDialog.this);
			}
		});

		builder.setNeutralButton(R.string.clear, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// User pressed clear, remove all devices that are not currently discovered

				ArrayList<UPnPDeviceInfo> unusedDevices = new ArrayList<UPnPDeviceInfo>(mDevices.size());
				for (UPnPDeviceInfo device: mDevices) {
					if (!device.isFound()) {
						unusedDevices.add(device);
					}
				}
				mDevices.removeAll(unusedDevices);
				
				//write the device preferences
				SharedPreferences sharedPreferences = getActivity().getPreferences(Activity.MODE_PRIVATE);
				SharedPreferences.Editor editor = sharedPreferences.edit();
				
				Set<String> preferredDevices = new HashSet<String>();
				for (UPnPDeviceInfo device: mDevices) {
					preferredDevices.add(device.getUDN()+"/"+device.getFriendlyName());
				}

				editor.putStringSet("UDN", preferredDevices);
				editor.commit();
				
				notifyDataSetChanged();
				
				//notify the host application of this dialog of the change
				mListener.onSelectionUpdated(DevicesDialog.this);
			}
		});



		Dialog dialog = builder.create();

		return dialog;
	}

	public void notifyDataSetChanged() {
		Log.e("TEMP","notifyDataSetChanged");
		if (mDeviceListAdapter!=null) mDeviceListAdapter.notifyDataSetChanged();
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	/* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface DialogNotificationListener {
        public void onSelectionUpdated(DialogFragment dialog);
    }
    
    // Use this instance of the interface to deliver action events
    DialogNotificationListener mListener;
    
    // Override the Fragment.onAttach() method to instantiate the DialogNotificationListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DialogNotificationListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }
	

}