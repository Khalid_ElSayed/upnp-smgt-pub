/* Copyright (c) 2013, TP Vision Holding B.V. 
 * All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of TP Vision nor the  names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL TP VISION HOLDING B.V. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

package com.tpvision.figaro;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import com.tpvision.figaro.DevicesDialog.DialogNotificationListener;
import com.tpvision.sensormgt.upnpcontrolpoint.clinginterface.UPnPDevice;
import com.tpvision.sensormgt.upnpcontrolpoint.model.SensorCollection;
import com.tpvision.sensormgt.upnpcontrolpoint.model.SensorCollection.SensorCollectionDataAvailableListener;
import com.tpvision.sensormgt.upnpcontrolpoint.model.SensorMgtDevice;
import com.tpvision.sensormgt.upnpcontrolpoint.model.SensorMgtDevice.SensorCollectionsChangedListener;
import com.tpvision.sensormgt.upnpcontrolpoint.upnpinterface.UPnPDeviceList.UPnPDeviceListListener;
import com.tpvision.uicomponent.CircleView;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnDismissListener;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class HomeControlGatewayActivity extends BaseActivity implements DialogNotificationListener, UPnPDeviceListListener, 
				SensorCollectionsChangedListener, SensorCollectionDataAvailableListener {
	private static final String TAG = "HomeControlGatewayActivity";
	private static final boolean DEBUG = true;
	
	private GridView gatewayItemsGridView;
	private HomeControlItemGridViewAdapter homeControlItemGridViewAdapter;
	private UPnPApplication  myApplication;
	private CustomDialog customizeDialog;
	private HomeControlItems mHomeControlItems = new HomeControlItems(true);
	
	
	private WifiLock wifiLock = null;
	private Boolean busyProcessing = false;
	ArrayAdapter<String> eventsAdapter = null;
	private List<String> events = null;
	private Dialog eventsDialog = null;
	private ListView eventListView = null;
	
	private Set<String> mPreferredDevices; //Set of upnp devices the user indicated to display in the UI
	private List<UPnPDeviceInfo> mUPnPDevices = new ArrayList<UPnPDeviceInfo>();
	
	private CircleView mDiscoveryStatus;
	private int mPercentageDetected=0;
	private TextView mStatusText;
	
	//TODO: add Event processing
	//TODO: bye bye and deleting of list
	//TODO: change UI HomeControlItems, add meta data
	//TODO: show energy readings
	//TODO: add getting extra meta-data

	//TODO: check wifi
	//TODO: fix optionsmenu
	
	//TODO: datastore interface
	//TODO: check if xml parsing needs to be replaced
	
	/**
	 * Check wifi, broadcast receiver 
	 * 
	 * 
	 */
	
	
	public void initWifiAndScreenLock()
	{
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifiLock = wm.createWifiLock("figaroWifilock");
        wifiLock.acquire();
    }
	
	
	
   
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        this.context = this;
        
        this.initWifiAndScreenLock();
        
        this.createEventsDialog();
        
        //setup gridview
        this.gatewayItemsGridView = (GridView)findViewById(R.id.gatewayItemsGridView);
        int itemWidth = this.context.getResources().getDrawable(R.drawable.item_background).getIntrinsicWidth();
        int margin = itemWidth /2;
        int numColumns = this.getScreenWidth() / (itemWidth + margin);
        
        this.gatewayItemsGridView.setNumColumns(numColumns);
      //  this.registerForContextMenu(this.gatewayItemsGridView);
        
        this.homeControlItemGridViewAdapter = new HomeControlItemGridViewAdapter(this.context, mHomeControlItems);
    	this.gatewayItemsGridView.setAdapter(homeControlItemGridViewAdapter );
    	this.gatewayItemsGridView.setOnItemClickListener(gridViewGalleryContentItemClick);    	
        
    	//setup discovery status led
    	this.mDiscoveryStatus = (CircleView) findViewById(R.id.circleView1);
    	this.mDiscoveryStatus.setOnClickListener(discoveryStatusClickListener);
    	this.mStatusText = (TextView) findViewById(R.id.statusText1);
    	setStatusLed(this.mDiscoveryStatus);
    	
        new Handler();
 //       new UPnPDevicesList();
        if (!Initialize()) {
        	return;
        }
        
        //get list of devices we want to use
        getPreferredDevices();
        
        myApplication.getUPnP().setDeviceListListener(this);

        if ((mPreferredDevices==null) || mPreferredDevices.isEmpty()) 
        	showDevicesDialog();
    }

    //doesn't seemed to be called with the noTitleBar theme. 
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main_menu, menu);
	    return true;
	}
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	        case R.id.select_devices:
	        	//issue a search, just to be sure the devices list is recent
	    //    	UPnPInit.search();
	        	showDevicesDialog();
	            return true;
	        case R.id.about:
//	            showHelp();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
    
    void showDevicesDialog() {

	    // DialogFragment.show() will take care of adding the fragment
	    // in a transaction.  We also want to remove any currently showing
	    // dialog, so make our own transaction and take care of that here.
	    FragmentTransaction ft = getFragmentManager().beginTransaction();
	    Fragment prev = getFragmentManager().findFragmentByTag("DevicesDialog");
	    if (prev != null) {
	        ft.remove(prev);
	    }
	    ft.addToBackStack(null);
  
	    // Create and show the dialog.
	    DevicesDialog newFragment = DevicesDialog.newInstance();   
	    
	    newFragment.setDiscoveredDevices(mUPnPDevices);
	    myApplication.getUPnP().search(); //trigger M-Search when we open the dialog
	    newFragment.show(ft, "DevicesDialog");
	}
    
    @Override
    protected void onRestart()
    {
    	super.onRestart();
    	initWifiAndScreenLock();
    }
    
    @Override
    protected void onStop()
    {    
    	super.onStop();
    	wifiLock.release();
    }
    
    @Override
    protected void onDestroy()
    {    
    	super.onDestroy();
    	Process.killProcess(Process.myPid());
    }
    
    //TODO: check what else to do
    @Override
    protected void onPause() 
    {
        super.onPause();
        wifiLock.release();
//        if (myApplication.isProgressDialogShown)
//        {
//            // progressDialog will leak when an orientation change occurs while it is shown.
//        	// Workaround = force dismissal of the progressDialog in onPause;
//        	progressDialog.dismiss();
//        	myApplication.isProgressDialogShown = false;
//        }
        finish();
    }
    

    @Override
    public void onResume()
    {    
    	super.onResume();
        wifiLock.acquire();
//    	if (controlpoint != null)
//    	{	        
//    		mHandler.post(new Runnable() 
//			{
//    			public void run() 
//	    		{
//    				//refreshDeviceList();
//				}
//    		});
//    	}
    }
    
    private OnClickListener discoveryStatusClickListener = new OnClickListener() {
		public void onClick(View v) {	
			showDevicesDialog();
		}
	};
    
    //TODO: trigger the event log somewhere else
    private OnClickListener philipsLogoImageViewOnClickListener = new OnClickListener() {
		public void onClick(View v) {	
			eventsDialog.show();
		}
	};
	
	private void createEventsDialog() {
		this.events = new ArrayList<String>();
        this.eventsAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, android.R.id.text1, events);        
		
        this.eventsDialog = new Dialog(context);
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Node events");		
		
		this.eventListView = new ListView(context);			
		this.eventListView.setAdapter(eventsAdapter);		
		this.eventListView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
		this.eventListView.setStackFromBottom(true);
		
		builder.setView(eventListView);
		this.eventsDialog.setCanceledOnTouchOutside(true);
		this.eventsDialog = builder.create();
	}
    
    private OnItemClickListener gridViewGalleryContentItemClick = new OnItemClickListener() {		
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {		
			if (!busyProcessing) {
				busyProcessing = true;
				HomeControlItem item = (HomeControlItem)homeControlItemGridViewAdapter.getItem(position);
				if (item.activate()) {
					homeControlItemGridViewAdapter.notifyDataSetChanged();				
				} else {
					showDetailsDialog(item);
				}
				busyProcessing = false;
			}
		}
	};
    
    public void FatalAlertDialog(String message) {
    	System.out.println(message);
		
		AlertDialog alertDialog;
		alertDialog = new AlertDialog.Builder(this).create();
	    alertDialog.setTitle(R.string.FATAL_ERROR);
	    alertDialog.setMessage(message);
	    alertDialog.setButton(getString(R.string.CLOSE_APPLICATION), new DialogInterface.OnClickListener() {
	    	public void onClick(DialogInterface dialog, int which) {
	    		Process.killProcess(Process.myPid());
	    		return;
	    	} 
	    });
	    alertDialog.show();
	}
    
    private Runnable mSetStatusLedRunnable = new Runnable() {

		@Override
		public void run() {
			setStatusLed(mDiscoveryStatus);
		}
	};
	
	public void setStatusLed(CircleView led) {
		if (mPercentageDetected == 100) {
			mDiscoveryStatus.setColor(getResources().getColor(R.color.green));
			mStatusText.setText(R.string.discovery_status_green);
		} else
		if (mPercentageDetected == 0) {
			mDiscoveryStatus.setColor(getResources().getColor(R.color.red));
			mStatusText.setText(R.string.discovery_status_red);
		} else {
			mDiscoveryStatus.setColor(getResources().getColor(R.color.yellow));
			mStatusText.setText(R.string.discovery_status_yellow);
		}
	}
    
    
    
    /************************************************************************************
     * 							Begin Context Menu	     								*
     ***********************************************************************************/
    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
      if (v.getId()==R.id.gatewayItemsGridView) {    	  
    	  menu.setHeaderTitle("Actions Menu");
    	  menu.add(Menu.NONE, 0, 0, "Details");
		  menu.add(Menu.NONE, 1, 1, "Bindings");    	  
      }
    }    
    
    @Override
    public boolean onContextItemSelected(MenuItem item) 
    {
    	AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
    	switch (item.getItemId())
    	{
    		case 0:    			
    			showDetailsDialog((HomeControlItem)homeControlItemGridViewAdapter.getItem(info.position));
    			break;
    		case 1:
    			showBindingsDialog((HomeControlItem)homeControlItemGridViewAdapter.getItem(info.position));
    			break;
    	}
    	return true;
    }
    
    private void showDetailsDialog(HomeControlItem homeControlItem)
	{
		customizeDialog = new CustomDialog(this, homeControlItem);
		customizeDialog.setOnDismissListener(customizeDialogOnDismissListener);
		customizeDialog.show();
	}	
	
	private void showBindingsDialog(HomeControlItem homeControlItem)
	{
		HomeControlItems items = new HomeControlItems();
		items.items.addAll(homeControlItemGridViewAdapter.items.items);
		BindingDialog d = new BindingDialog(this, homeControlItem, items);
		d.show();
	}
	
	private OnDismissListener customizeDialogOnDismissListener = new OnDismissListener() 
	{
	
		public void onDismiss(DialogInterface dialog) 
		{
			HomeControlItem item = ((CustomDialog)dialog).homeControlItem;			
			if (item.supportsRGB())
			{
				item.supportedActions = ((CustomDialog)dialog).rgbControl.homeControlItem.supportedActions;
			}			
			int pos = HomeControlItem.getItemPosByNodeId(homeControlItemGridViewAdapter.items.items, item.nodeId);			
			homeControlItemGridViewAdapter.items.items.set(pos, item);
			homeControlItemGridViewAdapter.notifyDataSetChanged();
		}
	};
    
	/************************************************************************************
     * 							End Context Menu	     								*
     ***********************************************************************************/    

    
    /***********************************************************************************
     * 							Begin UPnP Related functions						   *
     ***********************************************************************************/
    private boolean Initialize()
    {
    	myApplication = (UPnPApplication) getApplication();
    	myApplication.setActivity(this);
    	
    	UPnPApplication.InitState initState = myApplication.getInitState();
    	
    	switch (initState)
    	{
			case NO_ERROR:
//				controlpoint = myApplication.getControlpoint();
    			// always re-add listener 
    			// as this might be a new activity instance  
 //   	    	myApplication.addUPnPEventListener(this);
    	    	return true;
    	    		
    		case ERROR_ASSET_FILE_COPY_INT_SPACE_FAILED:
				FatalAlertDialog(getString(R.string.ERROR_ASSET_FILE_COPY_INT_SPACE_FAILED));    			
				return false;
    		
    		case ERROR_ASSET_FILE_COPY_EXT_SPACE_FAILED:
    			FatalAlertDialog(getString(R.string.ERROR_ASSET_FILE_COPY_EXT_SPACE_FAILED));    			
    			return false;
    			
    		case ERROR_NO_IP_ADDRESS:
    			FatalAlertDialog(getString(R.string.ERROR_NO_IP_ADDRESS));
    			return false;
    		
    		case ERROR_STARTING_UPNP_STACK :
    			FatalAlertDialog(getString(R.string.ERROR_STARTING_UPNP_STACK));
    			return false;
    		
    		case ERROR_STARTING_UPNP_CONTROL_POINT:
    			FatalAlertDialog(getString(R.string.ERROR_STARTING_UPNP_CONTROL_POINT));
    			return false;
    		
    		case ERROR_STARTING_WEBSERVER:
    			FatalAlertDialog(getString(R.string.ERROR_STARTING_WEBSERVER));
    			return false;
    		
    		default:
				FatalAlertDialog(getString(R.string.ERROR_UNKNOWN));
				return false;
    	}    	
    }
    
    /**
	 * Retrieves a list of UPnP devices previously selected by the user
	 */
	public void getPreferredDevices() {
		// Get preference list of devices we want to show in the UI
	    SharedPreferences settings = getPreferences(MODE_PRIVATE);
	    mPreferredDevices = settings.getStringSet("UDN", new HashSet<String>());
	    
	    //add all the devices we previously selected to the list, and set them to not discovered yet
	    if ((mPreferredDevices!=null) && !mPreferredDevices.isEmpty()) {
	    	Iterator<String> it = mPreferredDevices.iterator();
	    	while (it.hasNext()) {
	    		String preferredDevice = it.next();
	    		String split[] = preferredDevice.split("/");
	    		if (split.length==2) {
	    			UPnPDeviceInfo upnpDevice = new UPnPDeviceInfo(split[0],split[1], false, true);
	    			mUPnPDevices.add(upnpDevice);
	    			upnpDevice.setSensorCollectionChangedListener(this); //just add always, when the device is not set to use, no updates will be sent
	    			upnpDevice.setSensorCollectionDataAvailableListener(this);
	    		}
	    	}
	    } 
	}
	
	
	/** 
	 * refresh discovered UPnPDevices
	 */
	public void refreshDevices() {
		
		//get all discovered devices
		ArrayList<UPnPDevice> discoveredDevices = myApplication.getUPnP().getDevices();
		
		for (UPnPDevice device: discoveredDevices) {
			UPnPDeviceInfo registeredDevice = findUPnPDevice(mUPnPDevices, device.getUDN().getIdentifierString());
			if (registeredDevice!=null) {
				registeredDevice.setUPnPDevice(device);
				registeredDevice.setfound(true);		
				registeredDevice.populateDatamodel();
			} else {		
				UPnPDeviceInfo upnpDevice = new UPnPDeviceInfo(device);
				mUPnPDevices.add(upnpDevice);
				upnpDevice.setSensorCollectionChangedListener(this); //just add always, when the device is not set to use, no updates will be sent
				upnpDevice.setSensorCollectionDataAvailableListener(this);
				upnpDevice.enableEventSubscription(true);
				upnpDevice.populateDatamodel();
			}
		}
		
		//update the list in the dialog, if it is currently showing
		runOnUiThread(mDeviceListChangedRunnable);
	}

	public UPnPDeviceInfo findUPnPDevice(List<UPnPDeviceInfo> devices, String UDN) {
		Log.d("Temp","findUPnPDevice "+UDN);
		Iterator<UPnPDeviceInfo> it = devices.iterator();
		boolean found = false;
		UPnPDeviceInfo device=null;
    	while (it.hasNext()&&!found) {
    		device = it.next();
    		Log.d("Temp","find "+device.getFriendlyName()+", "+device.getUDN());

    		found = (UDN!=null) && (UDN.contentEquals(device.getUDN()));
    	}
    	
    	if (found) return device;
    	else return null;
	}
	
	public int percentagePreferredDevicesDetected(List<UPnPDeviceInfo> devices) {
		
		int countPreferredWanted=0;
		int countPreferredFound=0;
		 
		for (UPnPDeviceInfo device: devices) {
			if (device.isUsed()) {
				countPreferredWanted++;
				if (device.isFound())
					countPreferredFound++;
			}
		}
		
		int percentage=0;
		if(countPreferredWanted!=0) {
			percentage = countPreferredFound*100/countPreferredWanted;
		}
		
		return percentage;  
	}
	
	private Runnable mDeviceListChangedRunnable = new Runnable() {

		@Override
		public void run() {
			//if the dialog is showing
			DevicesDialog dialog = (DevicesDialog) getFragmentManager().findFragmentByTag("DevicesDialog");
			if (dialog != null) { 
		    	dialog.notifyDataSetChanged();
		    }
		}
	};
	
	private Runnable mUpdateGridView = new Runnable() {
		
		@Override
		public void run() {
			homeControlItemGridViewAdapter.notifyDataSetChanged();
		}
	};
	
	//check if we detected all devices were interested in, and update the statusled
	private void checkDiscoveredStatus() {
		
		mPercentageDetected = percentagePreferredDevicesDetected(mUPnPDevices);
		runOnUiThread(mSetStatusLedRunnable);
	}
    
    /***********************************************************************************
     * 							Begin UPnP Event Handlers    						   *
     **********************************************************************************/
    

	
//	public void cnNodeEvent(final String[] info) 
//	{
//		try
//		{
//			if (!initializationComplete)
//			{
//				return;
//			}
//			//events.add(info[1]);
//			this.eventsAdapter.add(info[1]);			
//			String[] splitted = info[1].split(";");
//			for (HomeControlItem homeControlItem: this.homeControlItemGridViewAdapter.items.items)
//			{
//				if (homeControlItem.nodeId.equals(splitted[0]))
//				{
//					double r = 0;
//					double g = 0;
//					double b = 0;
//					HA_ActionTypeEnum action = HA_ActionTypeEnum.valueOf(splitted[1]);				
//					switch (action)
//					{
//						case VALUE_CHANGED:
//							String[] splittedRGB = splitted[2].split(",");
//							r = Double.valueOf(splittedRGB[0]);
//							g = Double.valueOf(splittedRGB[1]);
//							b = Double.valueOf(splittedRGB[2]);
//							homeControlItem.setCurrentValue(ParameterIdEnum.R, r);
//							homeControlItem.setCurrentValue(ParameterIdEnum.G, g);
//							homeControlItem.setCurrentValue(ParameterIdEnum.B, b);
//							if ((0==r) & (0==g) & (0==b))
//							{
//								homeControlItem.setOn(false);								
//							}else
//							{
//								homeControlItem.setOn(true);
//							}
//							break;
//						case ON:
//							//homeControlItem.setOn(true);
//							break;
//						case OFF:
//							//homeControlItem.setOn(false);
//							break;
//						case TOGGLE:
//							if (homeControlItem.deviceBindingType!= DeviceBindingTypeEnum.SINK)
//							{
//								homeControlItem.setOn(!homeControlItem.isOn());
//							}
//							break;
//						case CHANGE_NAME:
//						case BIND:
//						case UNBIND:
//							homeControlItem.updateNodeInfo();
//							break;
//					}
//					if (this.customizeDialog!=null)
//					{
//						if (this.customizeDialog.isShowing() & (this.customizeDialog.homeControlItem.nodeId.equals(homeControlItem.nodeId)))
//						{
//							this.customizeDialog.homeControlItem = homeControlItem;							
//						}
//					}
//					mHandler.post(new Runnable()
//					{
//						public void run() 
//						{							
//							homeControlItemGridViewAdapter.notifyDataSetChanged();							
//							eventsAdapter.notifyDataSetChanged();														
//							if (customizeDialog!=null)
//							{
//								if (customizeDialog.isShowing())
//								{
//									customizeDialog.updateUI();
//								}
//							}
//							if (eventsDialog.isShowing())
//							{
//								eventListView.invalidate();
//								eventListView.smoothScrollToPosition(eventsAdapter.getCount()-1);
//							}
//						}
//					});
//					break;
//				}
//			}
//		}catch(Exception e)
//		{}
//	}

	@Override
	public void onSelectionUpdated(DialogFragment dialog) {
		if (DEBUG) Log.d (TAG, "onSelectionUpdated");
		refreshDevices();
		checkDiscoveredStatus();
		
	}

	@Override
	public void onDeviceListChanged() {
		if (DEBUG) Log.d (TAG, "onDeviceListChanged");
		refreshDevices();
		checkDiscoveredStatus();
	}


	@Override
	public void onSensorCollectionsChanged(SensorMgtDevice device) {
		if (DEBUG) Log.e (TAG, "onSensorCollectionChanged");
		
//		List<SensorCollection> sensorCollections = device.getSensorCollections();
//		for (SensorCollection sc: sensorCollections) {
//			mHomeControlItems.addItem(sc);
//		}
//		homeControlItemGridViewAdapter.notifyDataSetChanged();
	}




	@Override
	public void onSensorCollectionDataAvailable(SensorCollection sensorCollection) {
		if (DEBUG) Log.e (TAG, "onSensorCollectionDataAvailable "+sensorCollection.getPath());
		mHomeControlItems.addItem(sensorCollection);
		
		runOnUiThread(mUpdateGridView);
	}

	
	
	/***********************************************************************************
    * 							End UPnP Event Handlers    							   *
    ***********************************************************************************/
	/***********************************************************************************
    * 							End UPnP Related functions						       *
    ***********************************************************************************/
}