/* Copyright (c) 2013, TP Vision Holding B.V. 
 * All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of TP Vision nor the  names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL TP VISION HOLDING B.V. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

package com.tpvision.figaro;


import com.tpvision.sensormgt.upnpcontrolpoint.upnpinterface.UPnPInit;
import com.tpvision.sensormgt.upnpcontrolpoint.upnpinterface.UPnPNetworkInfo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.os.Process;

public class UPnPApplication extends Application 
{
	private UPnPInit UPnP= new UPnPInit();
//	private UPnPControlPoint controlpoint;
//	private UPnPCGIServer cgiServer;  
	public boolean isSearchTextFirstTime;
	public boolean isProgressDialogShown;
	private boolean IPAddressConfigured = false;
  
	public enum InitState 
	{
		NO_ERROR,
		ERROR_ASSET_FILE_COPY_INT_SPACE_FAILED,
		ERROR_ASSET_FILE_COPY_EXT_SPACE_FAILED,
		ERROR_NO_IP_ADDRESS,
		ERROR_STARTING_UPNP_STACK,
		ERROR_STARTING_UPNP_CONTROL_POINT,
		ERROR_STARTING_WEBSERVER
	}
 
	private InitState initState;
	private Activity activity;
  
	public  void setActivity(Activity activity)
	{
		this.activity = activity;
	}
  
	private BroadcastReceiver connectionEventReceiver = new BroadcastReceiver() 
	{
		boolean noIPConnectivity = false;
        
		@Override
		public void onReceive(Context context, Intent intent) 
		{
			// Monitor the Wifi connection state. 
			// In case IP traffic is no longer possible, shutdown the app
      
			// Note that even though the WifiManager might reconnected to a 
			// different AP automatically, we can not continue
			// (still need to kill the UPnP Stack in order to be able to correctly
			// rebind to a different IP)
          
			// TODO : Use the CPMediaServer_Android's WifiConnectivity.java
			// class here instead of this code ...                 

			if(intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION))
			{
				NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
				if ( !networkInfo.isConnected())
				{
					System.out.println("(NETWORK) WIFI DISCONNECTED .......... ");
					noIPConnectivity = true;
				}
			}
			else if(intent.getAction().equals(WifiManager.WIFI_STATE_CHANGED_ACTION))
			{
				int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_ENABLED);
				if (state != WifiManager.WIFI_STATE_ENABLED)
				{
					System.out.println("(WIFI) WIFI DISCONNECTED .......... ");
					noIPConnectivity = true;
				}
			}
  //FIXME: hack
			noIPConnectivity = false;
			if (noIPConnectivity)
			{
				String message;
          
				if (UPnPApplication.this.IPAddressConfigured)
				{
					message = "Lost network connection to the Wifi network. Please restart the application.";
				}
				else
				{
					message = "Please connect to a Wifi network and then restart the application.";
				}
          
				AlertDialog alertDialog;
				alertDialog = new AlertDialog.Builder(activity).create();
				alertDialog.setTitle("Fatal Error");
				alertDialog.setMessage(message);
				alertDialog.setButton("Close Application", new DialogInterface.OnClickListener() 
					{
						public void onClick(DialogInterface dialog, int which) 
						{
							Process.killProcess(Process.myPid());
							return;
						} 
					});
				alertDialog.show();
			}
		}
	};
  
  public InitState getInitState()
  {
    return initState;
  }
  
  public UPnPInit getUPnP() {
    return UPnP;
  }

//  public UPnPControlPoint getControlpoint() {
//    return controlpoint;
//  }
    
//    public String getCGIServerURL()
//    {
//      return cgiServer.getCGIServerURL(UPnP);
//    }  

  @Override
  public void onCreate() 
  {
	  super.onCreate();      
      isSearchTextFirstTime  = true;
      isProgressDialogShown = false;
      initState = InitState.NO_ERROR;
      
      if (initState == InitState.NO_ERROR)
      {      
    	  UPnP = new UPnPInit();
    //	  controlpoint = new UPnPControlPoint();
   // 	  cgiServer = new UPnPCGIServer();    
    	  initStack();
      }
      
      if (initState == InitState.NO_ERROR)
      {      
    	  RegisterWifiEventReceiver();
      }
   	}

    @Override
    public void onTerminate()
    {
        super.onTerminate();
        shutdown();
    }
    
    public String getHostIPaddress()
    {
    	boolean initialized = UPnP.isInitialized();
      
    	if (initialized)
    	{
    		return UPnP.getHostIPaddress();
    	}
    	return new String("unknown");
    }
    
    public String getHostPort()
    {
    	boolean initialized = UPnP.isInitialized();      
    	if (initialized)
    	{
    		return Integer.toString(UPnP.getHostPort());
    	}    	
		return new String("unknown");    	
    }
    
//    public void addUPnPEventListener(IUPnPControlPointEvents listener)
//    {
//    	if (controlpoint != null)
//    	{
//    		controlpoint.addUPnPEventListener(listener);
//    	}
//    }
    
    public InitState initStack()
    {
    	String IPaddress = UPnPNetworkInfo.getLocalIpAddress(); //getPreferredIPaddress();
     
    	if ((initState == InitState.NO_ERROR) && (IPaddress.length() == 0))
    	{
    		initState = InitState.ERROR_NO_IP_ADDRESS; 
    	}
    	else
    	{
    		IPAddressConfigured = true;
    	}
    
    	// Note that the call to InitializeUPnP
    	// will determine the IP address that the stack will bind to.
    	// Subsequent changes to available network interfaces 
    	// will currently _not_ result in correct rehosting on different IPs.    
    	// (This would require the stack to do a complete deinit - 
    	// init cycle which is currently not supported)    
    	if ((initState == InitState.NO_ERROR) && (!InitializeUPnP(IPaddress)))
    	{
    		initState = InitState.ERROR_STARTING_UPNP_STACK;
    	}    
    	if (initState == InitState.NO_ERROR) 
    	{      
//    		controlpoint = StartControlPoint(null);      
//    		if (controlpoint == null)
//		    {
//    			initState = InitState.ERROR_STARTING_UPNP_CONTROL_POINT;
//		    }
    	}    
//    	if ((initState == InitState.NO_ERROR) && (!startCGIServer(webdir)))
//    	{
//    		initState = InitState.ERROR_STARTING_WEBSERVER;
//    	}    
    	return initState;
    }

    //TODO: check what cling needs to shutdown
    private void shutdown()
    {      
//    	if (controlpoint != null)
//        {
//    		controlpoint.removeUPnPEventListener();
//    		controlpoint.stopCP();
//    		controlpoint = null;
//        }
        if (UPnP != null)
        {
        	UPnP.Deinit();
        	UPnP = null;
        }
    }

//    private UPnPControlPoint StartControlPoint(String webDirectory)
//    {
//    	boolean isStarted = controlpoint.isStarted();
//      
//    	if (!isStarted)
//    	{      
//    		isStarted = controlpoint.startCP(webDirectory);
//    		if (isStarted)
//    		{
//    			return controlpoint;
//    		}
//    		else
//    		{
//    			return null;
//    		}
//    	}
//    	else
//    	{
//    		return controlpoint;
//    	}
//    } 
    
//    public boolean startCGIServer(String webdir)
//    {
//      boolean isStarted = cgiServer.isStarted();
//      
//      if (!isStarted)
//      {
//        return cgiServer.start(webdir);
//      }
//      else
//      {
//        return isStarted;
//      }
//    }
//   
    private static String intToIp(int i) 
    {
        return (i & 0xFF) + "." + ((i >> 8 ) & 0xFF) + "." + ((i >> 16 ) & 0xFF) + "." + ((i >> 24 ) & 0xFF);
    }
  
    private String getPreferredIPaddress()
    {
    	WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
    	WifiInfo wifiInfo = wifiManager.getConnectionInfo();
    	String WifiIP = "";
        
    	if (wifiInfo != null)
    	{
    		if (wifiInfo.getIpAddress() != 0)
    		{
    			WifiIP = intToIp(wifiInfo.getIpAddress());
    		}
    	}
    	return WifiIP;
    }
  
    private boolean InitializeUPnP(String IPaddress) {
    	
    	boolean initialized = UPnP.isInitialized();      
	    if (!initialized)
	    {
	    	Log.d("UPnPApplication : IP address", IPaddress);
	        return UPnP.Init(getBaseContext(), IPaddress,0);	        
	    }
	    return initialized;	    
	}

	private void RegisterWifiEventReceiver()
	{
	     IntentFilter filter = new IntentFilter();     
	     filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
	     filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);      
	     registerReceiver(connectionEventReceiver, filter);
	}
}