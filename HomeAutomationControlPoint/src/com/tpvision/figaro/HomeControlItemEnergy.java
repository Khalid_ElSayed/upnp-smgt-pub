/* Copyright (c) 2013, TP Vision Holding B.V. 
 * All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of TP Vision nor the  names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL TP VISION HOLDING B.V. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

package com.tpvision.figaro;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.tpvision.figaro.ActionParameter.ParameterIdEnum;
import com.tpvision.figaro.SupportedAction.HA_ActionTypeEnum;
import com.tpvision.sensormgt.upnpcontrolpoint.model.DataItem;
import com.tpvision.sensormgt.upnpcontrolpoint.model.SensorCollection;
import com.tpvision.uicomponent.BargraphView;

public class HomeControlItemEnergy extends HomeControlItem {

	private static final String ENERGY = "Energy";

	private static final String TAG = "Energy";
	
	private DataItem mEnergyDataItem;

	private String mEnergyValue;
	private BargraphView mbargraphView;
	
	private HomeControlItems mParent;

	private HomeControlItemEnergy mThisHomeControl;
	
	
	public HomeControlItemEnergy(HomeControlItems parent, SensorCollection sc, String nodeId, DeviceBindingTypeEnum deviceBindingType, String friendlyname) {
		super(parent, sc, nodeId, DeviceTypeEnum.ENERGY, deviceBindingType, friendlyname);
		
		mParent = parent; 
		
		mEnergyDataItem = findDataItem(ENERGY);
		if (mEnergyDataItem!=null) { 
			mEnergyDataItem.addSensorDataItemReadListener(this);
			mEnergyDataItem.readSensorData();
		}
		
		mThisHomeControl = this;
	}
	
	public Boolean changeFriendlyName(String newName)
	{		
//		if (this.controlpoint!=null)
//		{			
//			if (this.controlpoint.sendMessage(this.currentUPnPDeviceUdn, CN_Networks_Enum.RF4CENetwork.name(), 
//					this.nodeId, HA_ActionTypeEnum.CHANGE_NAME.name(), newName))
//			{
//				this.friendlyname = newName;
//				return true;
//			}
//		}
		return false;
	}
	
	public boolean activate() {
		return toggle();
	}
	
	
	private boolean sendUPnPAction(HA_ActionTypeEnum actionType)
	{
//		if (this.controlpoint!=null)
//		{			
//			if (this.controlpoint.sendMessage(this.currentUPnPDeviceUdn, CN_Networks_Enum.RF4CENetwork.name(), 
//					this.nodeId,actionType.name(), SupportedAction.getFormatedPayload(this.supportedActions, actionType)))
//			{
//				return true;
//			}
//		}
		return false;
	}
	
	public double getMaxValue(ParameterIdEnum parameter)
	{

		return -1;
	}

	public static int getItemPosByNodeId(List<HomeControlItemEnergy> homeControlItems, String nodeId)
	{	
		try
		{
			for (int i=0;i<homeControlItems.size();i++)
			{
				if (homeControlItems.get(i).nodeId.equalsIgnoreCase(nodeId))
				{
					return i;
				}
			}
		}catch(Exception e)
		{}
		return -1;
	}
		
	@Override
	public View getView(final Context context, View convertView, OnLongClickListener friendlyNameTextViewOnLongClick, OnClickListener itemOnClickListener, HomeControlItem itemToBind) 
	{    

		ImageView itemImageView;
		TextView friendlyNameTextView;
		TextView valueTextView;

		if (convertView == null) 
		{
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);        	
			convertView = inflater.inflate(R.layout.home_control_item_energy, null);
		}

		itemImageView = (ImageView)convertView.findViewById(R.id.itemImageViewEnergy);
		
		
		
		

		friendlyNameTextView = (TextView)convertView.findViewById(R.id.friendlyNameTextView);        
		friendlyNameTextView.setText(this.friendlyname);
		if (friendlyNameTextViewOnLongClick!=null)
		{
			friendlyNameTextView.setOnLongClickListener(friendlyNameTextViewOnLongClick);
		}
		
		
		
		if(itemOnClickListener!=null)
		{
			itemImageView.setOnClickListener(itemOnClickListener);
		}

		//access dataItem to update the value
		if ((mEnergyDataItem!=null) && (mEnergyDataItem.getValue()!=null)) {
			mEnergyValue = mEnergyDataItem.getValue();
			valueTextView = (TextView) convertView.findViewById(R.id.valueTextView);
			valueTextView.setText(mEnergyValue);
			
			mbargraphView = (BargraphView) convertView.findViewById(R.id.bargraphView1);
			Float fval = Float.parseFloat(mEnergyValue);
			mbargraphView.setLevel(fval.intValue());
		}


		//itemImageView.setImageResource(HomeControlItemEnergy.DeviceTypeResourcesOn[this.devicetype.ordinal()]);


		return convertView;
	}
	
	
	@Override
	public void onSensorDataItemRead(DataItem dataItem) {
		Log.w(TAG,"DataItem event recieved from "+dataItem.getName());
		mEnergyValue = dataItem.getValue();
		mParent.onDataChanged(dataItem);
	}

	
	
	
}
