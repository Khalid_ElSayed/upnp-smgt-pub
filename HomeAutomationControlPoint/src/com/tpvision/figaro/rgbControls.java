/* Copyright (c) 2013, TP Vision Holding B.V. 
 * All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of TP Vision nor the  names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL TP VISION HOLDING B.V. BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

package com.tpvision.figaro;

import com.tpvision.figaro.ActionParameter.ParameterIdEnum;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;



public class rgbControls extends RelativeLayout 
{
	private OnRgbValueChangedListener onRgbValueChangedListener = null;
	public HomeControlItem homeControlItem;
	private ImageView colorWheelImageView;
	private ImageView colorSelectorImageView;
	private SeekBar brightnessSeekBar;
	private Bitmap colorWheelBitmap;
	private int colorWheelBitmapHeight=0;
	private int colorWheelBitmapWidth=0;
	private int[] colorWheelPixels;
	private int pixelColor;
	
	public static int MAX_COLOR_COMPONENT = 255;
	private int MAX_COLOR_COMPONENT_DEVICE = 4095;
	private double BRIGHTNESS_MULTIPLIER = 1;
	private Boolean trackingColor = false;
	
	private Boolean updatingUI = false;
	
	private Boolean initialized = false;
	
	private ColorRGB currentColor = new ColorRGB();
	
	public rgbControls(Context context, HomeControlItem homeControlItem) 
	{	
		super(context);
		initView(context, homeControlItem);
	}
	
	public rgbControls(Context context, AttributeSet attrs, HomeControlItem homeControlItem) 
	{
		super(context, attrs);
		initView(context, homeControlItem);
	}
	
	public rgbControls(Context context, AttributeSet attrs, int defStyle, HomeControlItem homeControlItem) 
	{
		super(context, attrs, defStyle);
		initView(context, homeControlItem);
	}
	
	private void initView(Context context, HomeControlItem homeControlItem)
	{		
		this.homeControlItem = homeControlItem;
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);		
		View view = inflater.inflate(R.layout.rgb_controls, this); 	
		
		colorWheelImageView = (ImageView)view.findViewById(R.id.colorWheelImageView);
		colorSelectorImageView = (ImageView)view.findViewById(R.id.colorSelectorImageView);
		brightnessSeekBar = (SeekBar) view.findViewById(R.id.brightnessSeekBar);
		brightnessSeekBar.setOnSeekBarChangeListener(brightnessSeekBarOnChangeListener);
		
		colorWheelBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.color_wheel_new);
		colorWheelImageView.setImageBitmap(colorWheelBitmap);		
		
		colorWheelImageView.setOnTouchListener(colorWheelOnTouchListener);		
		BRIGHTNESS_MULTIPLIER = (double)MAX_COLOR_COMPONENT_DEVICE /((double)MAX_COLOR_COMPONENT * brightnessSeekBar.getMax());
	}
	 
	public void setOnRgbValueChangedListener(OnRgbValueChangedListener listener) 
	{
		onRgbValueChangedListener = listener;
	}
	
	@Override	
	protected void dispatchDraw(Canvas canvas) 
	{	
		super.dispatchDraw(canvas);
		if (!initialized)
		{
			initialized = true;
			colorWheelBitmapHeight = colorWheelImageView.getMeasuredHeight();
			colorWheelBitmapWidth = colorWheelImageView.getMeasuredWidth();
			colorWheelPixels = new int[colorWheelBitmapHeight * colorWheelBitmapWidth];
			colorWheelBitmap.getPixels(colorWheelPixels, 0, colorWheelBitmapWidth, 0, 0, colorWheelBitmapWidth, colorWheelBitmapHeight);
			
			this.setMaxRGBValue((int)homeControlItem.getMaxValue(ParameterIdEnum.R));
			this.setColor(	(int)homeControlItem.getCurrentValue(ParameterIdEnum.R), 
							(int)homeControlItem.getCurrentValue(ParameterIdEnum.G), 
							(int)homeControlItem.getCurrentValue(ParameterIdEnum.B)
						);
		}
	}
	
	public void setColor(int R, int G, int B)
	{		
		int max = Math.max(Math.max(R, G), B);		
		if (max > MAX_COLOR_COMPONENT)
		{
			double brightness = 0;
			
			if (R==max)
			{
				brightness = R/MAX_COLOR_COMPONENT;
				this.currentColor.setRed(MAX_COLOR_COMPONENT);				
				this.currentColor.setGreen(G/brightness);
				this.currentColor.setBlue(B/brightness);
			}else if (G==max)
			{
				brightness = G/MAX_COLOR_COMPONENT;
				this.currentColor.setRed(R/brightness);				
				this.currentColor.setGreen(MAX_COLOR_COMPONENT);
				this.currentColor.setBlue(B/brightness);
			}else if (B==max)
			{
				brightness = B/MAX_COLOR_COMPONENT;
				this.currentColor.setRed(R/brightness);				
				this.currentColor.setGreen(G/brightness);
				this.currentColor.setBlue(MAX_COLOR_COMPONENT);
			}
			this.pixelColor = Color.rgb((int)this.currentColor.getRed(), (int)this.currentColor.getGreen(), (int)this.currentColor.getBlue());
			this.setBrightness(brightness);
		}else
		{
			this.currentColor.setRed(R);
			this.currentColor.setRed(G);
			this.currentColor.setRed(B);
			this.pixelColor = Color.rgb((int)this.currentColor.getRed(), (int)this.currentColor.getGreen(), (int)this.currentColor.getBlue());
			this.setBrightness(1);
		}
		Point p = this.getPointInCircleByColor((int)this.currentColor.getRed(), (int)this.currentColor.getGreen(), (int)this.currentColor.getBlue());
		if ((p.x >=0) & (p.y >=0))
		{
			this.setColorSelector(p);
		}
	}
	
	private void calculateRGB()
	{
		this.currentColor.setRed(Color.red(pixelColor));
		this.currentColor.setGreen(Color.green(pixelColor));
		this.currentColor.setBlue(Color.blue(pixelColor));
		this.homeControlItem.setCurrentValue(ParameterIdEnum.R, this.currentColor.getRed()*this.currentColor.getBrightness());
		this.homeControlItem.setCurrentValue(ParameterIdEnum.G, this.currentColor.getGreen()*this.currentColor.getBrightness());
		this.homeControlItem.setCurrentValue(ParameterIdEnum.B, this.currentColor.getBlue()*this.currentColor.getBrightness());
		if ((onRgbValueChangedListener!=null) & (!updatingUI))
		{
			onRgbValueChangedListener.OnRgbValueChanged(this.currentColor);
		}
	}
		
	private void setBrightness(double brightness)
	{
		this.currentColor.setBrightness(brightness);
		this.brightnessSeekBar.setProgress((int)(brightness/this.BRIGHTNESS_MULTIPLIER));		
	}
	
	private OnSeekBarChangeListener brightnessSeekBarOnChangeListener = new OnSeekBarChangeListener() 
	{			
		public void onStopTrackingTouch(SeekBar seekBar) 
		{
			trackingColor = false;			
		}
				
		public void onStartTrackingTouch(SeekBar seekBar) 
		{
			trackingColor = true;			
		}		
		
		public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) 
		{
			if (trackingColor)
			{
				currentColor.setBrightness( progress * BRIGHTNESS_MULTIPLIER );			
				calculateRGB();
			}
		}
	}; 
	
	private OnTouchListener colorWheelOnTouchListener = new OnTouchListener() 
	{
		public boolean onTouch(View v, MotionEvent event)
		{ 			
			int eventaction = event.getAction();
			int diameter = colorWheelBitmapWidth;									
			
			Point inCirle = new Point((int)event.getX(), (int)event.getY());
			inCirle = getPointInCircle(inCirle, new Point((diameter)/2, (diameter)/2), diameter/2);						
			
			switch (eventaction)
			{
				case MotionEvent.ACTION_DOWN:					
					break;
				
				case MotionEvent.ACTION_MOVE:					
					setColorSelector(inCirle);
					break;

				case MotionEvent.ACTION_UP:
					setColorSelector(inCirle);
					pixelColor = colorWheelBitmap.getPixel(inCirle.x + colorSelectorImageView.getWidth()/2, 
								inCirle.y + colorSelectorImageView.getHeight()/2);
					calculateRGB();
					break;
			}
		    return true;		    		    
		}
	};
	
	public ColorRGB getCurrentColor() 
	{
		return currentColor;
	}
	
	public double getBrightness()
	{
		return this.currentColor.getBrightness(); 
	}

	private Point getPointInCircle(Point p, Point center, int radius)
	{
		if (p.x<0)
		{
			p.x=0;		
		}
		else if (p.x > 2*radius-1)
		{
			p.x = 2*radius-1;
		}
		
		if (p.y<0)
		{
			p.y=0;
		}
		else if (p.y > 2*radius - 1)
		{
			p.y = 2*radius - 1;
		}
		
		double dist = Math.sqrt(Math.pow(p.x- center.x,2) + Math.pow(p.y- center.y,2)) - radius;
		Point out = p;
		if (dist>0)
		{
			double len = Math.sqrt(Math.pow(p.x- center.x,2) + Math.pow(p.y- center.y,2));
			out.x = (int) (center.x + radius * (p.x - center.x) / len);
			out.y = (int) (center.y + radius * (p.y - center.y) / len);						
		}
		return out;
	}
	
	private Point getPointInCircleByColor(int R, int G, int B)
	{		
		int inputColor = Color.rgb(R, G,B);		
		int y = -1;
		int x = -1;
		
		if (colorWheelPixels!=null)
		{
			double minColourDistance = 100;
			for (int i=0; i<colorWheelPixels.length; i++)
			{			
				double colourDistance = ColorRGB.ColourDistance(inputColor, colorWheelPixels[i]);
				if (colourDistance<minColourDistance)
				{
					minColourDistance = colourDistance;
					y = i/colorWheelBitmapWidth;				
					x = i - y*colorWheelBitmapWidth;
					if (minColourDistance<1)
					{
						break;
					}
				}
			}
		}
		return new Point(x, y);
	}
	
	private void setColorSelector(Point p)
	{
		LayoutParams layoutParams = (LayoutParams) colorSelectorImageView.getLayoutParams();
		layoutParams.leftMargin = p.x - colorSelectorImageView.getWidth()/2;
		layoutParams.topMargin = p.y - colorSelectorImageView.getHeight()/2;
		colorSelectorImageView.setLayoutParams(layoutParams);
		invalidate();
	}

	private void setMaxRGBValue(int maxRGB)
	{
		this.MAX_COLOR_COMPONENT_DEVICE = maxRGB;
	}
	
	public void updateUI()
	{
//		updatingUI = true;
//		this.setColor((int)homeControlItem.getCurrentValue(ParameterIdEnum.R), 
//				(int)homeControlItem.getCurrentValue(ParameterIdEnum.G), 
//				(int)homeControlItem.getCurrentValue(ParameterIdEnum.B)
//			);
//		updatingUI = false;
	}
}
